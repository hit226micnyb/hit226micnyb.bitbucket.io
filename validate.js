// Credit: https://www.educba.com/forms-in-javascript/


// Defining a function to validate form
function validateForm() {
// Retrieving the values of form elements
var name = document.registrationForm.username.value;
var address = document.registrationForm.address.value;

var email = document.registrationForm.usermail.value;
var pswd = document.registrationForm.password.value;
var mobile = document.registrationForm.telephone.value;
var dob = document.registrationForm.birthdate.value;

// Checking the age of the user. Display alert if needed
var dobDate = new Date(dob);
var currentDate = new Date();
var age = (currentDate - dobDate) / (1000 * 60 * 60 * 24 * 365);    // (1000 * 60 * 60 * 24 * 365) is calculating 1 year.
if(age < 13) {
    alert("Warning! Too Young");
} else {
if (age > 50) {
    alert("Do you need assistance? Visit our help section.");
} else {

// Creating a string from input data for preview
var dataPreview = "You've entered the following details: \n" +
"Full Name: " + name + "\n" +
"Address: " + address + "\n" +
"Email Address: " + email + "\n" +
"Password: " + pswd + "\n" +
"Mobile Number: " + mobile + "\n" +

"Birthdate: " + dob + "\n";

// Display input data in a dialog box before submitting the form
alert(dataPreview);

}}
};